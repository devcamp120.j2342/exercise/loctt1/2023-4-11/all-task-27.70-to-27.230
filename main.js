"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gCOMBO_SMALL = "S";
const gCOMBO_MEDIUM = "M";
const gCOMBO_LARGE = "L";
const gLOAI_GA = "Gà";
const gLOAI_HAI_SAN = "Hải sản";
const gLOAI_THIT_NUONG = "Thịt nướng";
const gBASE_URL_DRINK = "http://203.171.20.210:8080/devcamp-pizza365/drinks";
const gBASE_URL_VOURCHER =
  "http://203.171.20.210:8080/devcamp-pizza365/voucher_detail";
const gBASE_URL_CREATE = "http://203.171.20.210:8080/devcamp-pizza365/orders";
const gREQUEST_STATUS_OK = 200;
const gREQUEST_READY_STATUS_FINISH_AND_OK = 4;
const gREQUEST_CREATE_SUCCESS = 201; // status 201 - tạo thành công

/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
function onNavOrderDetailClick() {
  console.log("%cGọi chi tiết order ở đây", "color:blue");
  var vBtnOrderDetail = document.getElementById("btn-detail");
  var vOrderCode = vBtnOrderDetail.dataset.ordercode;
  var vId = vBtnOrderDetail.dataset.id;
  var vLink = "OrderDetailVersion1.html";
  window.location.href = vLink + "?orderCode=" + vOrderCode + "&" + "id=" + vId;
}
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// chon size
function onBtnSClick() {
  getCombo("S", "20cm", "2", "200g", "2", 150000);
  changeColorMenu("S");
}
// chon size
function onBtnMClick() {
  getCombo("M", "25cm", "4", "300g", "3", 200000);
  changeColorMenu("M");
}
// chon size
function onBtnLClick() {
  getCombo("L", "30cm", "8", "500g", "4", 250000);
  changeColorMenu("L");
}
// chọn loại pizza
function onBtnGaClick() {
  console.log("%c Loại pizza bạn chọn : pizza Gà", "color:red");
  changeColorLoaiPizza(gLOAI_GA);
}
// chọn loại pizza
function onBtnGaClick() {
  console.log("%c Loại pizza bạn chọn : pizza Gà", "color:red");
  changeColorLoaiPizza(gLOAI_GA);
}
// chọn loại pizza
function onBtnHaiSanClick() {
  console.log("%c Loại pizza bạn chọn : pizza Hải sản", "color:red");
  changeColorLoaiPizza(gLOAI_HAI_SAN);
}
// chọn loại pizza
function onBtnThitNuongClick() {
  console.log("%c Loại pizza bạn chọn : pizza Thịt nướng", "color:red");
  changeColorLoaiPizza(gLOAI_THIT_NUONG);
}
// load dữ liệu nước uống
function onPageLoadDrink() {
  var vXmlHttp = new XMLHttpRequest();
  goiApiDeLayDrink(vXmlHttp);
  vXmlHttp.onreadystatechange = function () {
    if (
      vXmlHttp.readyState == gREQUEST_READY_STATUS_FINISH_AND_OK &&
      vXmlHttp.status == gREQUEST_STATUS_OK
    ) {
      hienThiThongTinDrink(vXmlHttp);
      console.log(vXmlHttp.responseText);
    }
  };
}

// send đơn hàng
function onBtnSendDonHang() {
  var vDonHangObj = {
    kichCo: "",
    suon: "",
    salad: "",
    duongKinh: "",
    soLuongNuoc: "",
    priceVND: "",
    loaiPizza: "",
    hoTen: "",
    email: "",
    soDienThoai: "",
    diaChi: "",
    loiNhan: "",
    idVourcher: "",
    idLoaiNuocUong: "",
    donGiaNuoc: 0,
    thanhTien: 0,
    giamGia: 0,

    priceAnnualVND: function () {
      if (this.giamGia == 0) {
        this.thanhTien = this.priceVND;
        return this.thanhTien;
      } else {
        return (this.thanhTien = this.priceVND * (1 - this.giamGia / 100));
      }
    },
  };

  thuThapThongTinDonHang(vDonHangObj);
  vDonHangObj.priceAnnualVND();

  var vCheck = kiemTraThongTinDonHang(vDonHangObj);
  if (vCheck) {
    var vXmlHttp = new XMLHttpRequest();
    goiApiDeCheckIdVourcher(vDonHangObj, vXmlHttp);

    if (vXmlHttp.status == gREQUEST_STATUS_OK) {
      layPhanTramGiamGia(vDonHangObj, vXmlHttp);

      console.log(vXmlHttp.responseText);
    } else {
      alert("không có phần trăm giảm giá");
    }

    goiApiCreateOrder(vDonHangObj, vXmlHttp);
    vXmlHttp.onreadystatechange = function () {
      if (
        vXmlHttp.readyState == gREQUEST_READY_STATUS_FINISH_AND_OK &&
        vXmlHttp.status == gREQUEST_CREATE_SUCCESS
      ) {
        console.log(vXmlHttp.responseText);
        hienThiThongTinDonHang(vDonHangObj, vXmlHttp);
      }
    };
  }
}
// btn orderDetail

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
function getCombo(
  paramKichCo,
  paramDuongKinh,
  paramSuonNuong,
  paramSalad,
  paramDrink,
  paramPriceVND
) {
  var vComboObj = {
    kichCo: paramKichCo,
    duongKinh: paramDuongKinh,
    suon: paramSuonNuong,
    salad: paramSalad,
    soLuongNuoc: paramDrink,
    priceVND: paramPriceVND,
    displayInConsoleLog: function () {
      console.log("%c combo bạn chọn : " + paramKichCo, "color:red");
      console.log("Kích cỡ : " + paramKichCo);
      console.log("Đường kính : " + paramDuongKinh);
      console.log("sườn nướng : " + paramSuonNuong);
      console.log("Salad : " + paramSalad);
      console.log("Drink : " + paramDrink);
      console.log("Price VND : " + paramPriceVND);
    },
  };

  return vComboObj;
}
// đổi màu nút chọn
function changeColorMenu(paramSize) {
  var vBtnS = document.getElementById("btn-s");
  var vBtnM = document.getElementById("btn-m");
  var vBtnL = document.getElementById("btn-l");

  if (paramSize == gCOMBO_SMALL) {
    vBtnS.setAttribute("data-is-selected-menu", "Y");
    vBtnS.className = "btn btn-warning";

    vBtnM.setAttribute("data-is-selected-menu", "N");
    vBtnM.className = "btn btn-success";

    vBtnL.setAttribute("data-is-selected-menu", "N");
    vBtnL.className = "btn btn-success";
  } else if (paramSize == gCOMBO_MEDIUM) {
    vBtnS.setAttribute("data-is-selected-menu", "N");
    vBtnS.className = "btn btn-success";

    vBtnM.setAttribute("data-is-selected-menu", "Y");
    vBtnM.className = "btn btn-warning";

    vBtnL.setAttribute("data-is-selected-menu", "N");
    vBtnL.className = "btn btn-success";
  } else if (paramSize == gCOMBO_LARGE) {
    vBtnS.setAttribute("data-is-selected-menu", "N");
    vBtnS.className = "btn btn-success";

    vBtnM.setAttribute("data-is-selected-menu", "N");
    vBtnM.className = "btn btn-success";

    vBtnL.setAttribute("data-is-selected-menu", "Y");
    vBtnL.className = "btn btn-warning";
  }
}
// đổi màu loại pizza
function changeColorLoaiPizza(paramPizza) {
  var vBtnGa = document.getElementById("btn-ga");
  var vBntHaiSan = document.getElementById("btn-hai-san");
  var vBtnThitNuong = document.getElementById("btn-thit-nuong");
  if (paramPizza == gLOAI_GA) {
    vBtnGa.setAttribute("data-is-selected-pizza", "Y");
    vBtnGa.className = "btn btn-warning w-100";

    vBntHaiSan.setAttribute("data-is-selected-pizza", "N");
    vBntHaiSan.className = "btn btn-success w-100";

    vBtnThitNuong.setAttribute("data-is-selected-pizza", "N");
    vBtnThitNuong.className = "btn btn-success w-100";
  } else if (paramPizza == gLOAI_HAI_SAN) {
    vBtnGa.setAttribute("data-is-selected-pizza", "N");
    vBtnGa.className = "btn btn-success w-100";

    vBntHaiSan.setAttribute("data-is-selected-pizza", "Y");
    vBntHaiSan.className = "btn btn-warning w-100";

    vBtnThitNuong.setAttribute("data-is-selected-pizza", "N");
    vBtnThitNuong.className = "btn btn-success w-100";
  } else if (paramPizza == gLOAI_THIT_NUONG) {
    vBtnGa.setAttribute("data-is-selected-pizza", "N");
    vBtnGa.className = "btn btn-success w-100";

    vBntHaiSan.setAttribute("data-is-selected-pizza", "N");
    vBntHaiSan.className = "btn btn-success w-100";

    vBtnThitNuong.setAttribute("data-is-selected-pizza", "Y");
    vBtnThitNuong.className = "btn btn-warning w-100";
  }
}
// api gọi đồ uống
function goiApiDeLayDrink(paramXmlHttp) {
  paramXmlHttp.open("GET", gBASE_URL_DRINK, true);
  paramXmlHttp.send();
}

// hiển thị thông tin nước uống từ api
function hienThiThongTinDrink(paramXmlHttp) {
  var vSelectDrink = document.getElementById("select-drink");
  var vObjDrink = JSON.parse(paramXmlHttp.responseText);
  for (var bI = 0; bI < vObjDrink.length; bI++) {
    var vOptionDrink = document.createElement("option");
    vOptionDrink.value = vObjDrink[bI].maNuocUong;
    vOptionDrink.text = vObjDrink[bI].tenNuocUong;
    vSelectDrink.appendChild(vOptionDrink);
  }
}
// gọi api để check mã giảm giá
function goiApiDeCheckIdVourcher(paramDonHangObj, paramXmlHttp) {
  paramXmlHttp.open(
    "GET",
    gBASE_URL_VOURCHER + "/" + paramDonHangObj.idVourcher,
    false
  );
  paramXmlHttp.send();
}
// gọi api để gửi đơn hàng
function goiApiCreateOrder(paramOrderObj, paramXmlHttp) {
  var vJsonData = JSON.stringify(paramOrderObj);
  paramXmlHttp.open("POST", gBASE_URL_CREATE, true);
  paramXmlHttp.setRequestHeader(
    "Content-Type",
    "application/json;charset=UTF-8"
  );
  paramXmlHttp.send(vJsonData);
}
// lấy phần trăm giảm giá
function layPhanTramGiamGia(paramDonHangObj, paramXmlHttp) {
  var vObjVoucher = JSON.parse(paramXmlHttp.responseText);
  paramDonHangObj.giamGia = vObjVoucher.phanTramGiamGia;
}
// thu thập thông tin đơn hàng
function thuThapThongTinDonHang(paramDonHangObj) {
  var vInputHoTen = document.getElementById("inp-fullname");
  var vInputEmail = document.getElementById("inp-email");
  var vInpuDiaChi = document.getElementById("inp-dia-chi");
  var vInputSoDienThoai = document.getElementById("inp-dien-thoai");
  var vInputLoiNhan = document.getElementById("inp-message");
  var vInputMaGiamGia = document.getElementById("inp-voucher-id");

  paramDonHangObj.hoTen = vInputHoTen.value.trim();
  paramDonHangObj.email = vInputEmail.value.trim();
  paramDonHangObj.diaChi = vInpuDiaChi.value.trim();
  paramDonHangObj.soDienThoai = vInputSoDienThoai.value.trim();
  paramDonHangObj.loiNhan = vInputLoiNhan.value.trim();
  paramDonHangObj.idVourcher = vInputMaGiamGia.value.trim();

  var vSelectDrink = document.getElementById("select-drink");
  var vOptionDrink = vSelectDrink.options[vSelectDrink.selectedIndex].text;
  paramDonHangObj.idLoaiNuocUong = vOptionDrink;

  var vBtnS = document.getElementById("btn-s");
  var vBtnM = document.getElementById("btn-m");
  var vBtnL = document.getElementById("btn-l");
  var vBtnGa = document.getElementById("btn-ga");
  var vBntHaiSan = document.getElementById("btn-hai-san");
  var vBtnThitNuong = document.getElementById("btn-thit-nuong");
  if (vBtnS.getAttribute("data-is-selected-menu") == "Y") {
    paramDonHangObj.kichCo = "S";
    paramDonHangObj.suon = "2";
    paramDonHangObj.salad = "200g";
    paramDonHangObj.duongKinh = "20cm";
    paramDonHangObj.priceVND = 150000;
    paramDonHangObj.soLuongNuoc = "2";
  } else if (vBtnM.getAttribute("data-is-selected-menu") == "Y") {
    paramDonHangObj.kichCo = "M";
    paramDonHangObj.suon = "4";
    paramDonHangObj.salad = "300g";
    paramDonHangObj.duongKinh = "25cm";
    paramDonHangObj.priceVND = 200000;
    paramDonHangObj.soLuongNuoc = "3";
  } else if (vBtnL.getAttribute("data-is-selected-menu") == "Y") {
    paramDonHangObj.kichCo = "L";
    paramDonHangObj.suon = "8";
    paramDonHangObj.salad = "500g";
    paramDonHangObj.duongKinh = "30cm";
    paramDonHangObj.priceVND = 250000;
    paramDonHangObj.soLuongNuoc = "4";
  }

  if (vBtnGa.getAttribute("data-is-selected-pizza") == "Y") {
    paramDonHangObj.loaiPizza = "Pizza Gà";
  } else if (vBntHaiSan.getAttribute("data-is-selected-pizza") == "Y") {
    paramDonHangObj.loaiPizza = "Pizza Hải Sản";
    // paramDonHangObj.vLoaiPizza == "Pizza Hải sản";
  } else if (vBtnThitNuong.getAttribute("data-is-selected-pizza") == "Y") {
    // paramDonHangObj.vLoaiPizza == "Pizza Thịt nướng";
    paramDonHangObj.loaiPizza = "Pizza thịt nướng";
  }
}
// kiểm tra thông tin hợp lệ
function kiemTraThongTinDonHang(paramDonHangObj) {
  if (
    paramDonHangObj.hoTen == "" ||
    paramDonHangObj.soDienThoai == "" ||
    paramDonHangObj.diaChi == ""
  ) {
    alert("Các trường không được bỏ trống!");
    return false;
  } else if (checkEmail(paramDonHangObj.email) == false) {
    return false;
  } else if (paramDonHangObj.kichCo == "") {
    alert("Phải chọn menu");
    return false;
  } else if (paramDonHangObj.loaiPizza == "") {
    alert("Phải chọn loại pizza");
    return false;
  } else if (paramDonHangObj.idLoaiNuocUong == "Chọn nước uống") {
    alert("chọn nước uống");
    return false;
  }
  return true;
}
// kiểm tra email hợp lệ
function checkEmail(paramEmail) {
  var filter =
    /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  if (!filter.test(paramEmail)) {
    alert("Hay nhap dia chi email hop le.\nExample@gmail.com");
    email.focus;
    return false;
  } else {
    alert("Email nay hop le.");
  }
}
// hiển thị thông tin
function hienThiThongTinDonHang(paramDonHangObj, paramXmlHttp) {
  var vObjOrder = JSON.parse(paramXmlHttp.responseText);
  var vDivShow = document.getElementById("div-show-don-hang");
  var vPText = document.getElementById("p-text");
  vDivShow.style.display = "block";
  vPText.innerHTML =
    "Cảm ơn bạn đã đặt hàng tại Pizza 365." +
    "</br>" +
    "Mã đơn hàng của bạn là: " +
    vObjOrder.orderCode;

  var vBtnOrderDetail = document.getElementById("btn-detail");
  vBtnOrderDetail.setAttribute("data-ordercode", vObjOrder.orderCode);
  vBtnOrderDetail.setAttribute("data-id", vObjOrder.id);
}
